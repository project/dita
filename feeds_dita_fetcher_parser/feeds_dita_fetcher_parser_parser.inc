<?php
// $Id$

class FeedsDITAParser extends FeedsParser {
   public function parse(FeedsImportBatch $batch, FeedsSource $source) {
    if (method_exists($batch, 'getMultiRaw')) {
      $result = dita_fetcher_parser_parse($batch->getMultiRaw());
    }
    else {
      $result = dita_fetcher_parser_parse($batch->getRaw());
    }
    $batch->setTitle($result['title']);
    $batch->setDescription($result['description']);
    $batch->setLink($result['link']);
    if (is_array($result['items'])) {
      foreach ($result['items'] as $item) {
        $batch->addItem($item);
      }
    }
   }
}

/**
 * Builds an array from the given array for the processor
 *
 * @param  array $strings contains a list of strings
 * @return array result array for the processor
 */
function dita_fetcher_parser_parse($strings) {
  if (!is_array($strings)) {
    watchdog('Feeds_dita_fetcher_parser_debug', '$strings is not an array!', $strings);
    $strings = array($strings);
  }
  $result['title'] = 'This is a dita feed - ' . time();
  $result['teaser'] = NULL;
  $result['description'] = 'This is a dita feed';
  $result['link'] = NULL;

  $items = array();
  
  //@todo is it good here?
  module_load_include('inc', 'dita', 'dita.filter');

  foreach ($strings as $string) {
    $item = array();
    $xml_object = new SimpleXMLElement($string);
    $item['title'] = (string) array_shift($xml_object->xpath('/*/title[1]'));
    $item['description'] = $string;
    $item['author_name'] = 'dita module';
    if (isset($news['pubDate'])) { //@TODO filedate
      $item['timestamp'] = 0;
    }
    else {
      $item['timestamp'] = time();
    }
    $item['url'] = 'http://example.com';
    $item['guid'] = 999;
    $item['geolocations'] = array(); //@TODO WTF is that?

    //no description means the dita file was invalid or empty so we skip it
    if (!empty($item['description'])) {
      $items[] = $item;
    }
  }
  $result['items'] = $items;
  return $result;
}
