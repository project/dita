<?php
// $Id$

/**
 * Fetches a files from a specified directory.
 *
 * This fetcher will keep track of the files that it has already fetched, and
 * will not re-fetch them, unless specifically told to. //@TODO test it!
 *
 * This fetcher will only work parsers, that can handle class FeedsMultiFileBatch
 * wich returns items in an array, such like this modules own parser, the DITA Parser
 */
class FeedsDitaFetcherParserFetcher extends FeedsFileFetcher {

 /**
   * Fetch content from a source and return it.
   *
   * Every class that extends FeedsFetcher must implement this method.
   *
   * @param $source
   *   Source value as entered by user through sourceForm().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    //watchdog('feeds_directory_fetcher_sourcecheckcheck', 'feeds sourcecheck',array($source_config));
    $dir = file_create_path($source_config['source']);
    //quickfix for double slashing
    if (substr($dir, -1) == '/') {
      $dir = substr($dir, 0, -1);
    }
    $files = file_scan_directory($dir, ".*");
    $filepaths = array();
    foreach ($files as $file) {
      $filepath = $file->filename;
      if (isset($source_config['feed_files_fetched'][$filepath])) {
        continue;
      }
      $source_config['feed_files_fetched'][$filepath] = TRUE;
      $blah = $source->getConfig();
      $blah[get_class($this)] = $source_config;
      $source->setConfig($blah);

      $filepaths[] = ($filepath);
    }

    if (!empty($filepaths)) {
      return new FeedsMultiFileBatch($filepaths);
    }
    // Return an empty FeedsImportBatch if we didn't get anything from the directory:
    return new FeedsImportBatch();
  }

 /**
   * Source form.
   */
  public function sourceForm($source_config) {
    $form = array();
    // When renaming, do not forget feeds_vews_handler_field_source class.
    $form['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Directory'),
      '#description' => t('Specify a directory in the filesystem to scan for feed data.'),
      '#default_value' => isset($source_config['source']) ? $source_config['source'] : '',
    );
    $form['reset'] = array(
      '#type' => 'checkbox',
      '#title' => t('Re-fetch entire directory'),
      '#description' => t('When checked will re-fetch previously imported data.'),
      '#default_value' => 0,
    );
    $form['feed_files_fetched'] = array(
      '#type' => 'value',
      '#value' => isset($source_config['feed_files_fetched']) ? $source_config['feed_files_fetched'] : array(),
    );
    return $form;
  }

 /**
   * Override parent::sourceFormValidate().
   */
  public function sourceFormValidate(&$values) {
    // If $values['source'] is not empty, make
    // sure that this directory is within Drupal's files directory as otherwise
    // potentially any file that the web server has access could be exposed.
    $path = $values['source'];
    print "path = " . $path;
    if (!file_create_path($path)) {
      form_set_error('feeds][source', t('Directory needs to point to a directory in your Drupal file system path.'));
    }
    if ($values['reset']) {
      $values['feed_files_fetched'] = array();
      $values['reset'] = 0;
    }
  }

 /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'feed_files_fetched' => array(),
    );
  }
}

class FeedsMultiFileBatch extends FeedsFileBatch {
  protected $file_path_array;

  public function __construct($file_path_array, $feed_nid = 0) {
    $this->file_path_array = $file_path_array;
    parent::__construct('', $feed_nid);
  }

  /**
   * Implementation of FeedsImportBatch::getRaw();
   */
  public function getRaw() {
    return file_get_contents(realpath($this->file_path_array[0]));
  }

  public function getMultiRaw() {
    //This is a new function, and can't be handled by the feeds' default parsers
    $strings = array();
    foreach ($this->file_path_array as $file_path) {
      $strings[] = file_get_contents(realpath($file_path));
    }
    return $strings;
  }
}
