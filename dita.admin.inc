<?php
// $Id$

/**
 * Page callback for admin/content/dita.
 * Callback for drupal_get_form().
 */
function dita_admin_form() {
  $f = array();

  foreach (dita_get_output_converters() as $converter) {
    $varname = call_user_func(array($converter, 'getVariableName'));
    if (!empty($varname)) {
      $f[$varname] = array(
        '#title' => call_user_func(array($converter, 'getWorkingDirectoryName')),
        '#description' => t('Please provide an absolute path.'),
        '#type' => 'textfield',
        '#default_value' => variable_get($varname, ''),
      );
    }
  }

  $f['dita_output_dir'] = array(
    '#title' => t('Output directory'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dita_output_dir', ''),
  );

  $f['dita_web_dir'] = array(
    '#title' => t('Output dir in the basepath'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dita_web_dir', ''),
  );

  $f['dita_basedir'] = array(
    '#title' => t('DITA basedir'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dita_basedir', ''),
  );

  return system_settings_form($f);
}

/**
 * Validation callback for 'dita_admin_form'.
 */
function dita_admin_form_validate($form, &$form_state) {
  foreach (dita_get_output_converters() as $converter) {
    $varname = call_user_func(array($converter, 'getVariableName'));
    if (!empty($varname) && !call_user_func(array($converter, 'validateWorkingDirectory'),
        $form_state['values'][$varname])) {
      form_set_error($varname, t('Invalid @dirname', array(
        '@dirname' => call_user_func(array($converter, 'getWorkingDirectoryName')),
      )));
    }
  }
  foreach (array('dita_output_dir', 'dita_basedir') as $d) {
    if (!is_writable($form_state['values'][$d])) {
      form_set_error($d, t('Directory "' . $form_state['values'][$d] . '" is not writable.'));
    }
  }
}

function dita_admin_remote_form() {
  $f = array();

  $f['dita_remote_endpoint'] = array(
    '#title' => t('Remote endpoint'),
    '#description' => t('Services endpoint of the remote DITA conversion service'),
    '#type' => 'textfield',
    '#default_value' => variable_get('dita_remote_endpoint', NULL),
  );

  return system_settings_form($f);
}

function _dita_task_status_to_string($status) {
  switch ($status) {
    case 0:
      return t('In queue');
    case 1:
      return t('Completed successfully');
    default:
      return t('Failed: %error-code', array(
        '%error-code' => $status >> 1,
      ));
  }
}

function dita_admin_export_page($node = NULL) {
  module_load_include('inc', 'dita');

  $nid = (is_object($node) && isset($node->nid)) ? $node->nid : 0;

  $headers = array(
    t('Map name'), t('Reltable vocabulary'), t('Status'), t('Link'),
    '&nbsp;',
  );
  $rows = array();

  $res = pager_query('SELECT * FROM {dita_task} WHERE nid = %d OR 0 = %d ORDER BY dtid DESC',
    10, 0, NULL, $nid, $nid);
  while ($row = db_fetch_object($res)) {
    $node = node_load($row->nid);
    $handler = dita_get_output_handler($row->type, $row);
    $rows[] = array(
      $node->title,
      dita_taxonomy_vocabulary_name($row->vocabularies),
      _dita_task_status_to_string($row->status),
      $row->status == 1 ? l(t('Output'), $handler->getOutputDocumentPath()) : t('No output'),
      ($row->status > 1) ?
        l(t('Reschedule'), 'admin/content/dita/reschedule/' . $row->dtid):
        '&nbsp;',
    );
  }

  return theme('table', $headers, $rows) . theme('pager');
}

/**
 * Callback for admin/content/dita/reschedule/%
 */
function dita_admin_reschedule_form($form_state, $dtid) {
  $form = array();

  $form['dtid'] = array(
    '#type' => 'value',
    '#value' => $dtid,
  );

  return confirm_form($form,
    t('Are you sure that you want to reschedule the task?'),
    'admin/content/dita/export',
    t('Only do it if you corrected the error'),
    t('Reschedule'), t('Cancel'));
}

function dita_admin_reschedule_form_submit($form, &$form_state) {
  db_query('UPDATE {dita_task} SET status = 0 WHERE dtid = %d',
    $form_state['values']['dtid']);

  drupal_set_message('Task is rescheduled.');

  drupal_goto('admin/content/dita/export');
}
