<?php

define('MESSAGE_NO_XHTML_OUTPUT', 'No Xhtml output could be generated');
// $Id$

/**
 * Preview for DITA XML page, wich prints out the convered dita topic
 *
 * @param object $node
 * @return string
 */
function simple_dita_forms_preview_page($node) {
  drupal_add_css(drupal_get_path('module', 'dita') .'/css/dita_xml_view.css');
  module_load_include('inc', 'dita', 'dita.export');
  $output = dita_format_xml_string(convert_simple_dita2dita($node));
  if (!$output) {
    return 'Convert failed!';
  }
  else {
    return '<div class="dita_source">' . htmlentities($output) . '</div>';
  }
}

/**
 * Page callback for dita topic's XHTML view. Return with node_show for non-dita
 * nodes.
 *
 * OBSOLETE!
 * @TODO remove
 *
 * @param object $node
 * @param int $cid
 * @return string
 */
function simple_dita_forms_node_page_view($node, $cid = NULL) {
  //if the node is a content type provided by simple dita forms, we override the
  //default page callback
  if (simple_dita_forms_is_sdf($node->type)) {
    /*@todo function wich converts a simple dita forms dita node to chtml
     return with return value*/
    $dita_xml = convert_simple_dita2dita($node);
    $output = dita_filter_transform($dita_xml);
    if ($output) {
      return $output;
    }
    else {
      return MESSAGE_NO_XHTML_OUTPUT;
    }
  }
  else {
    //original page callback for node view
    drupal_set_title(check_plain($node->title));
    return node_show($node, $cid);
  }
}
