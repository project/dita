<?php
/**
 * Implements hook_drush_command()
 */
function dita_dir_synchroniser_drush_command() {
  $commands['dita-dir-print-hash-map'] = array(
    'description' => 'Prints out a hash map',
     'options' => array(
      '--R' => 'Recursive.' ,
      '--path' => 'Directory to process',
    ),
  );
  $commands['dita-dir-add'] = array(
    'description' => 'Prints out a hash map',
     'options' => array(
      '--R' => 'Recursive.' ,
      '--path' => 'Directory to process',
    ),
  );
  $commands['dita-dir-del'] = array(
    'description' => 'Prints out a hash map',
     'options' => array(
       '--did' => 'ID of the directory to delete',
    ),
  );
  return $commands;
}

/**
 * Implements dita-dir-print-hash-map command
 */
function drush_dita_dir_print_hash_map() {
  if (!module_exists('dita_dir_synchroniser')) {
    return drush_set_error(DRUSH_FRAMEWORK_ERROR, dt(
            'DITA Directory Synchroniser module is not enabled.'));
  }
  $extensions = array('xml', 'dita');
  $recursive = (bool) drush_get_option('R', FALSE);
  $path = drush_get_option('path', '');
  if (!(bool)$path) {
    return drush_set_error(DRUSH_FRAMEWORK_ERROR, dt(
            'Usage: drush dita-dir-print-hash-map [options] --path=[path] .'));
  }
  $hash_map = dita_dir_synchroniser_create_hash_map($path, $extensions, $recursive, array('valid'));
  var_dump($hash_map);
}

/**
 * Implements dita-dir-add
 */
function drush_dita_dir_add() {
  if (!module_exists('dita_dir_synchroniser')) {
    return drush_set_error(DRUSH_FRAMEWORK_ERROR, dt(
            'DITA Directory Synchroniser module is not enabled.'));
  }
  $extensions = array('xml', 'dita');
  $recursive = (bool) drush_get_option('R', FALSE);
  $path = drush_get_option('path', '');
  if (!(bool)$path) {
    return drush_set_error(DRUSH_FRAMEWORK_ERROR, dt(
            'Usage: drush dita-dir-add [options] --path=[path] .'));
  }
  $hash_map = dita_dir_synchroniser_create_hash_map($path, $extensions, $recursive, array('valid'));
  dita_dir_synchroniser_add_dir($hash_map, $path, $recursive);
}

/**
 * Implements dita-dir-del
 */
function drush_dita_dir_del() {
  if (!module_exists('dita_dir_synchroniser')) {
    return drush_set_error(DRUSH_FRAMEWORK_ERROR, dt(
            'DITA Directory Synchroniser module is not enabled.'));
  }
  $did = drush_get_option('did', '');
  if (!(bool)$did) {
    return drush_set_error(DRUSH_FRAMEWORK_ERROR, dt(
            'Usage: drush dita-dir-del [options] --did=[did] .'));
  }
  dita_dir_synchroniser_delete_dir($did);
}