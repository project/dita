<?php
// $Id$

define('DITA_TO_EMEDDED_XHTML_DIR', 'libs/xslt');
define('DITA_TO_EMEDDED_XHTML_FILENAME', 'dita2xhtml_emb.xsl');

//array legyen benne minden tipus
// http://docs.oasis-open.org/dita/v1.2/os/spec/non-normative/dtdorganization.html

/**
 * Return the available
 *
 * @global string $base_url
 * @todo Allow alter and extend the list with hooks.
 * @todo Test if all elements are correct
 *
 * @return array
 *   key =>
 *     doctype => array
 *       list of the name of root elements of this type. E.g. array('concept') for Concepts.
 *     public identifier => DITA external subset public identifier
 *     system identifier => DITA external subset system identifier
 */
function dita_get_topic_types($type = NULL) {
  global $base_url;
  $dita_path = $base_url .'/'. drupal_get_path('module', 'dita');
  $topic_types = array(
    'concept' => array(
      'doctype' => array (
         'concept',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Concept//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/concept.dtd',
    ),
    'composite' => array(
      'doctype' => array(
        'composite', //Not sure if it's correct
       ),
      'public_identifier' => '-//OASIS//DTD DITA Composite//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/ditabase.dtd',
    ),
    'glossary_entry' => array(
      'doctype' => array(
        'glossentry',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Glossary Entry//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/glossentry.dtd',
    ),
    'glossary' => array(
      'doctype' => array(
        'glossary',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Glossary//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/glossary.dtd',
    ),
    'reference' => array(
      'doctype' => array(
        'reference',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Reference//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/reference.dtd',
    ),
    'task' => array(
      'doctype' => array(
        'task',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Task//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/task.dtd',
    ),
    'general_task' => array(
      'doctype' => array(
        'generalTask',
       ),
      'public_identifier' => '-//OASIS//DTD DITA General Task//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/generalTask.dtd',
    ),
    'topic' => array(
      'doctype' => array(
        'topic',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Topic//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/topic.dtd',
    ),
    'base_topic' => array(
      'doctype' => array(
        'basetopic',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Base Topic//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/basetopic.dtd',
    ),
    'machinhery_task' => array(
      'doctype' => array(
        'machineryTask',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Machinery Task//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/machineryTask.dtd',
    ),
    'learning_assessment' => array(
      'doctype' => array(
        'learningAssessment',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Learning Assessment//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/learningAssessment.dtd',
    ),
    'learning_content' => array(
      'doctype' => array(
        'learningContent',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Learning Content//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/learningContent.dtd',
    ),
    'learning_overview' => array(
      'doctype' => array(
        'learningOverview',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Learning Overview//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/learningOverview.dtd',
    ),
    'learning_plan' => array(
      'doctype' => array(
        'learningPlan',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Learning Plan//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/learningPlan.dtd',
    ),
    'learning_summary' => array(
      'doctype' => array(
        'learningSummary',
       ),
      'public_identifier' => '-//OASIS//DTD DITA Learning Summary//EN',
      'system_identifier' => $dita_path . '/dtd1.2/technicalContent/dtd/learningSummary.dtd',
    ),
  );
  return $type ? $topic_types[$type] : $topic_types;
  return $topic_types;
}
/**
 * Transforms a dita topic to xhtml
 *
 * @param string $xml valid xml string
 * @return string valid xml
 */
function dita_filter_transform($xml) {
  if (!$xml) {
    return FALSE;
  }
  module_load_include('export.inc', 'dita');

  $xslt_dir = drupal_get_path('module', 'dita') . '/' . DITA_TO_EMEDDED_XHTML_DIR;
  if (file_exists($xslt_dir . '/'. DITA_TO_EMEDDED_XHTML_FILENAME)) {
    $xml = dita_apply_xslt($xml, file_get_contents($xslt_dir .
            '/'. DITA_TO_EMEDDED_XHTML_FILENAME), TRUE, $xslt_dir);
  }
  else {
    drupal_set_message('XSLT file not found!', 'error');
  }
  return $xml;
}


/**
 * Sets the subset identifiers for a dita topic, to avoid problems with relative
 * paths to the dtds
 *
 * Use only for XSLT transformations (XSLTProcessor requires valid DTD in the
 *   target xml)
 *
 * @param string $xml valid xml string
 * @param string if not FALSE, forces the doctype to be 'concept'/'reference'/'task'
 * @return string valid xml string
 */
function _dita_rewrite_dtd($xml, $forced_doctype = FALSE) {
  module_load_include('export.inc', 'dita');

  if (!$forced_doctype) {
    $doctype = dita_get_doctype($xml);
  }
  else {
    $doctype = $forced_doctype;
  }

  $topic_types = dita_get_topic_types();

  foreach ($topic_types as $type => $definition) {
    if (in_array($doctype, $definition['doctype'])) {
      $public_id = $definition['public_identifier'];
      $system_id = $definition['system_identifier'];
      break;
    }
  }

  if ((!isset($public_id)) || (!isset($system_id))) {
    return FALSE;
  }

  $xml_object = @dita_create_xml_object($xml, $doctype, $public_id, $system_id);
  if (!$xml_object) {
    return FALSE;
  }
  return $xml_object->saveXML();
}

/**
 * Helper function to validate a given XML with an XSD.
 *
 * @param string $xml
 * @param string $schema_path
 * @return array Empty array is valid.
 */
function _dita_validate_xsd($xml, $schema_path) {
  libxml_clear_errors();

  $dom = new DOMDocument();
  $dom->loadXML($xml);
  //@todo set path?
  $dom->schemaValidate($schema_path);

  return libxml_get_errors();
}

/**
 * Function checks if a string is a valid DITA Topic.
 *
 * @param string $xml XML Document supposed to be a valid DITA Topic
 * @return array
 *   return value of libxml_get_errors()
 */
function dita_is_invalid($xml) {
  return _dita_validate_xsd($xml, dita_get_schema_path());
}

/**
 * Function checks if a string is a valid DITA map.
 *
 * @param string $xml XML Document supposed to be a valid DITA Map
 * @return array
 *   return value of libxml_get_errors()
 */
function dita_map_is_invalid($xml) {
  return _dita_validate_xsd($xml, dita_get_mapschema_path());
}
