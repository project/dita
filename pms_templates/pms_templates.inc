<?php
/**
 * Converter class for simple dita forms
 */
class PmsTemplateConverter extends SimpleDitaDocument {
  public function __construct($dita_node) {
    module_load_include('inc', 'content', 'includes/content.token');

    $template_node = pms_templates_get_template($dita_node->type);

    $body = token_replace($template_node->body, 'node', $dita_node);
    $dom = new DOMDocument();
    $dom->loadXML($body);

    //note: available func: dita_create_xml_object(), dita_get_doctype()
    $this->xml_object = $dom;
  }
}
