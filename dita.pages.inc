<?php
// $Id$

function dita_upload_form($form_state) {
  $f = array();

  $f['#attributes'] = array('enctype' => 'multipart/form-data');

  $f['dita_topic'] = array(
    '#type' => 'file',
    '#title' => t('DITA topic file'),
  );

  $f['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );

  return $f;
}

function dita_upload_form_submit($form, &$form_state) {
  global $user;
  module_load_include('filter.inc', 'dita');
  $validators = array();

  $file = file_save_upload('dita_topic', $validators, file_directory_path());

  if (!$file) {
    drupal_set_message(t('File upload failed'), 'error');
    return;
  }

  $drupal_path = explode('/', $_SERVER['SCRIPT_FILENAME']);
  array_pop($drupal_path);
  $drupal_path = implode('/', $drupal_path) . '/';

  $absolute_path = $drupal_path . $file->filepath;

  $body = file_get_contents($absolute_path);
  $body = $body;

  $node = new stdClass();
  $node->type = 'dita';
  $node->status = 1;
  $node->promote = 1;
  $node->body = $body;
  $node->uid = $user->uid;
  $node->name = $user->name;

  // running validator
  $node_form = array();
  $node_form['body_field']['body']['#value'] = $body;

  if (!dita_validate($node, $node_form)) {
    return;
  }

  $node = node_submit($node);
  node_save($node);

  if ($node->nid > 0) {
    drupal_goto('node/' . $node->nid);
  }
}

/**
 * @todo Document
 */
function ditamap_preview_page($node) {
  ctools_add_js('ajax-responder');
  drupal_add_js(drupal_get_path('module', 'dita') . '/dita.ctools.js');
  drupal_add_css(drupal_get_path('module', 'dita') . 'css/dita.css');
  module_load_include('inc', 'dita');

  $items = array();

  foreach (array(0 => t('No reltable')) + dita_taxonomy_all_vocabularies()
      as $vid => $name) {
    $items[] = l($name, "node/{$node->nid}/ditamap/nojs/$vid", array(
      'attributes' => array(
        'class' => "ctools-use-ajax ditamap-vid ditamap-vid-$vid",
      ),
    ));
  }

  return theme('item_list', $items) . theme('ditamap_preview', '');
}

function ditamap_preview_page_ajax($node, $js = FALSE, $vid = 0) {
  module_load_include('export.inc', 'dita');

  $vids = isset($_SESSION['ditamap_preview_vids_' . $node->nid]) ?
    $_SESSION['ditamap_preview_vids_' . $node->nid] :
    array();

  if ($vid == 0) {
    $vids = array();
  }
  else {
    if (in_array($vid, $vids)) {
      foreach ($vids as $key => $value) {
        if ($value == $vid) {
          unset($vids[$key]);
        }
      }
    }
    else {
      $vids[] = $vid;
    }
  }

  $_SESSION['ditamap_preview_vids_' . $node->nid] = $vids;

  $xml_string =  dita_convert_map_node($node, $vids);
  $xml_string = dita_format_xml_string($xml_string);
  $output = theme('ditamap_preview', $xml_string);

  if ($js) {
    ctools_include('ajax');
    module_load_include('ctools.inc', 'dita');

    $commands = array();
    $commands[] = ctools_ajax_command_after('#ditamap-in-xml', $output);
    $commands[] = ctools_ajax_command_remove('#ditamap-in-xml:first');
    $commands[] = ctools_ajax_command_removeclass('.ditamap-vid', 'ditamap-vid-highlight');

    foreach ($vids as $v) {
      $commands[] = ctools_ajax_command_addclass(".ditamap-vid-$v", 'ditamap-vid-highlight');
    }

    ctools_ajax_render($commands);
  }
  else {
    return $output;
  }
}

function ditamap_export_page($node) {
  return drupal_get_form('ditamap_export_form', $node);
}

function ditamap_export_form($form_state, $node) {
  module_load_include('inc', 'dita');
  $f = array();

  $f['node'] = array(
    '#type' => 'value',
    '#value' => $node,
  );

  $f['output_format'] = array(
    '#type' => 'radios',
    '#options' => drupal_map_assoc(array_keys(dita_get_output_types())),
    '#title' => t('Output format'),
    '#default_value' => 'pdf2',
  );

  $f['vocabularies'] = array(
    '#type' => 'checkboxes',
    '#options' => dita_taxonomy_all_vocabularies(),
    '#title' => t('Reltable vocabulary'),
  );

  $f['priority'] = user_access('set dita export priority') ?
    array(
      '#type' => 'select',
      '#default_value' => 0,
      '#title' => t('Priority'),
      '#options' => drupal_map_assoc(range(-2, 2)),
    ):
    array(
      '#type' => 'value',
      '#value' => 0,
    );

  $conversion = array(
    'local' => t('Local'),
  );
  $conversion_default = 'local';

  if (variable_get('dita_remote_endpoint', NULL)) {
    $conversion['remote'] = t('Remote');
    $conversion_default = 'remote';
  }

  $f['conversion'] = array(
    '#title' => t('Conversion'),
    '#type' => 'radios',
    '#default_value' => $conversion_default,
    '#options' => $conversion,
  );

  $f['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Export'),
  );

  return $f;
}

function ditamap_export_form_validate($form, &$form_state) {

}

function ditamap_export_form_submit($form, &$form_state) {
  module_load_include('export.inc', 'dita');

  $basedir = rtrim(variable_get('dita_basedir', ''), '/');
  $output_dir = variable_get('dita_output_dir', '');

  $exporter = new DITAProjectExporter($form_state['values']['node']);
  $exporter->setBasedir($basedir);
  $vocabularies = array();
  foreach ($form_state['values']['vocabularies'] as $vid => $checked) {
    if ($checked) {
      $vocabularies[] = $vid;
    }
  }
  $ditamap = $exporter->__invoke($vocabularies);

  db_query('INSERT INTO {dita_task}
    (ditamap, nid, prefix, type, output_dir, basedir, enabled, status, priority)
    VALUES(\'%s\', %d, \'%s\', \'%s\', \'%s\', \'%s\', %d, 0, %d)',
    $ditamap, $form_state['values']['node']->nid,
    $exporter->prefix,
    $form_state['values']['output_format'], $output_dir,
    $exporter->getBasedir(),
    $form_state['values']['conversion'] == 'local',
    $form_state['values']['priority']);

  $dtid = db_last_insert_id('dita_task', 'dtid');

  foreach ($form_state['values']['vocabularies'] as $vid => $checked) {
    if ($checked) {
      db_query('INSERT INTO {dita_task_vocabulary}(dtid, vid) VALUES(%d, %d)',
        $dtid, $vid);
    }
  }

  switch ($form_state['values']['conversion']) {
    case 'local':
      drupal_set_message(t('DITA document is scheduled to conversion.'));
      break;

    case 'remote':
      $endpoint = variable_get('dita_remote_endpoint', NULL);
      $files = array();
      $rawfiles = drupal_map_assoc(
        glob("{$basedir}/{$exporter->prefix}/*", GLOB_MARK), 'file_get_contents');
      foreach ($rawfiles as $name => $content) {
        $files[substr($name, strlen($basedir))] = base64_encode($content);
      }

      $conn = xmlrpc($endpoint, 'system.connect');
      if ($conn) {
        $conn = (object) $conn;
        $result = xmlrpc($endpoint, 'dita.convert', $conn->sessid,
               $files, $exporter->prefix, $form_state['values']['output_format'],
               url('services/xmlrpc', array('absolute' => TRUE)));
        if (!xmlrpc_errno()) {
          switch ($result) {
            case DITA_SERVER_RESPONSE_NO_MAPFILE:
              drupal_set_message(t('Failed to locate the ditamap file.'), 'error');
              break;
            case DITA_SERVER_RESPONSE_OK:
              drupal_set_message(
                t('DITA document is successfully sent to the remote server for conversion'));
              break;
            default:
              drupal_set_message(t('Unknown error.'), 'error');
              break;
          }
        }
        else {
          drupal_set_message(
            t('Failed to send the DITA document to the remote server. ' .
              'The error was: %code: %message', array(
                '%code' => xmlrpc_errno(),
                '%message' => xmlrpc_error_msg(),
              )),
          'error');
        }
      }
      else {
        drupal_set_message(t('Failed to connect to the remote server.'), 'error');
      }
      break;
  }

  drupal_goto('node/' . $form_state['values']['node']->nid . '/ditamapexport/overview');
}

/**
 * Page callback of dita_download/all
 */
function dita_download_page() {
  return l('Download all DITA topics in zip format', 'dita_download/zip');
}

/**
 * Page callback of dita_download/zip
 */
function dita_download_zip() {

  module_load_include('inc', 'dita', 'dita.export');
  $topics = dita_get_topics();

  $topics = array_merge(dita_get_simple_dita_nodes(), $topics);

  $file = dita_create_zipfile($topics);

  header('Content-Type: application/zip');
  header('Content-Length: ' . filesize($file));
  header('Content-Disposition: attachment; filename="dita_nodes.zip"');
  readfile($file);
  unlink($file);

  return NULL;
}

/**
 * Page callback of node/%node_id/dita_xml
 *
 * Prints out the dita source, wich is the same as the body, but formatted
 * and replaced doctype
 */
function dita_preview_page($node) {
  module_load_include('inc', 'dita', 'dita.filter');
  drupal_add_css(drupal_get_path('module', 'dita') .'/css/dita_xml_view.css');
  if (is_object($node)&&(isset($node->nid))) {
    $output =  $node->body;
    if ($output) {
      return '<div class="dita_source">' . htmlentities($node->body) . '</div>';
    }
    else {
      drupal_set_message('Not a valid DITA document!', 'error');
      return 'Display failed!';
    }
  }
  else {
    drupal_set_message('Not a valid node!', 'error');
    return 'Display failed!';
  }
}
