<?php
// $Id$

define('DITA_PROJECT_PREFIX_LENGTH', 6);
define('DITA_TOPIC_PREFIX', '');
define('DITA_TOPIC_SUFFIX', '.dita');
define('DITAMAP_DOCTYPE_STRING', '<!DOCTYPE map PUBLIC "-//OASIS//DTD DITA Map//EN"
 "http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/technicalContent/dtd/map.dtd">');
define('DITAMAP_DOCTYPE', 'map');
define('DITAMAP_EXTERNAL_SUBSET_PUBLIC_IDENTIFIER',
  '-//OASIS//DTD DITA Map//EN');
define('DITAMAP_EXTERNAL_SUBSET_SYSTEM_IDENTIFIER',
  'http://docs.oasis-open.org/dita/v1.2/os/dtd1.2/technicalContent/dtd/map.dtd');

/**
 * Applies an xslt schema to an xml file
 *
 * @param string $xml_string the xml
 * @param string $xslt_string the xslt
 * @param string working_dir Change working dir for the transformation. If false
 *               no directory change applies
 * @return string the output after conversion
 */
function dita_apply_xslt($xml_string, $xslt_string, $external_dtd = FALSE, $working_dir = FALSE) {
  module_load_include('filter.inc', 'dita');

  if (!$xml_string) {
    return FALSE;
  }

  if (!$xslt_string) {
    drupal_set_message('No xslt given in dita_apply_xslt', 'warning');
    return $xml_string;
  }

  if ($rewritten_xml = _dita_rewrite_dtd($xml_string)) {
    $xml_string = $rewritten_xml;
  }

  $orig_dir = getcwd();

  if ($working_dir) {
    chdir($working_dir);
  }

  $xml_object = new DOMDocument();
  if ($external_dtd) {
    $xml_object->loadXML($xml_string, LIBXML_DTDATTR);
  }
  else {
    $xml_object->loadXML($xml_string);
  }
  if (!$xml_object) {
    if ($working_dir) {
      chdir($orig_dir);
    }
    return FALSE;
  }

  $xslt_object = new DOMDocument();
  $xslt_object->loadXML($xslt_string);
  if (!$xslt_object) {
    if ($working_dir) {
      chdir($orig_dir);
    }
    return FALSE;
  }

  $xslt_processor = new XSLTProcessor;
  $xslt_processor->importStylesheet($xslt_object);
  if (!$xslt_processor) {
    if ($working_dir) {
      chdir($orig_dir);
    }
    return FALSE;
  }

  if ($working_dir) {
    chdir($orig_dir);
  }
  return $xslt_processor->transformToXML($xml_object);
}

/**
 * Creates a domdocument object from an xml string, with the desired
 * doctpye declaration
 *
 * @param string $xml input xml string
 * @param string $doctype the qualified name of the document type to create
 * @param string $public_id the external subset public identifier
 * @param string $system_id the external subset system identifier
 * @return object DomDocument object
 */

function dita_create_xml_object($xml, $doctype, $public_id, $system_id) {
  //insert doctype
  $implementation = new DOMImplementation();

  $dtd = $implementation->createDocumentType($doctype,
        $public_id,
        $system_id);

  $xml_object = $implementation->createDocument('', '', $dtd);

  $map_original = new DomDocument();
  $map_original->loadXML($xml);

  $child = $xml_object->importNode($map_original->documentElement, TRUE);
  $xml_object->appendChild($child);
  return $xml_object;
}

/**
 * Do the indentation of an XML file (if tidy extension is loaded)
 *
 * @param string
 *   Broken or not complete, wrongly indented xml
 * @return string
 *   The formatted XML
 */
function dita_format_xml_string($xml) {
  if (extension_loaded('tidy')) {
    $config = array(
      'indent' => TRUE,
      'output-xml' => TRUE,
      'input-xml' => TRUE,
      'wrap' => '1000'
    );

    $tidy = new tidy();
    $tidy->parseString($xml, $config, 'utf8');
    $tidy->cleanRepair();
    $xml = tidy_get_output($tidy);
  }

  return $xml;
}

/**
 * Creates a a string wich can be a machine-readable filename from a random string.
 * Practically that means it removes any non-alphabetical characters, and replaces
 * whitespaces to '_'
 *
 * @param string $str input string
 * @return string simplified string
 */
function dita_str_to_filename($str) {
  $return = '';
  if (empty($str)) {
    return '';
  }
  $str = str_split($str);
  foreach ($str as $key => $char) {
    $ord_char = ord($char);
    if ((dita_check_interval($ord_char, 48, 57))||
       (dita_check_interval($ord_char, 65, 90))||
       (dita_check_interval($ord_char, 97, 122))) {
      $return .= $char;
    }
    //convert whitespaces to -
    elseif ($char == ' ') {
      $return .= '-';
    }
  }
  return $return;
}


/**
 * Checks if a given number is in the given closed interval
 *
 * @param int $num The number
 * @param int $bottom lower limit point of interval
 * @param int $top upper limit point of the interval
 * @return bool if the point is in the interval
 */
function dita_check_interval($num, $bottom, $top) {
  return ($num >= $bottom) && ($num <= $top);
}

/**
 *
 * @param array $reltable 3D array for the reltable
 * @param array $relheader list of rlospecs (content types)
 * @param bool $indent=FALSE if set, the output will be regurally indented
 *
 * @return string a formatted ditamap xml string
 */
function dita_render_reltable(array $reltable, array $relheader, $indent = FALSE) {
  if (!count($reltable) || !count($relheader)) {
    return NULL;
  }

  $dom = new DOMDocument();
  $dom->formatOutput = (bool) $indent;
  $domreltable = $dom->createElement('reltable');

  // header
  $domrelheader = $dom->createElement('relheader');
  foreach ($relheader as $rolspec) {
    $rs = $dom->createElement('relcolspec');
    $rs->setAttribute('type', $rolspec);
    $domrelheader->appendChild($rs);
    unset($rs);
  }
  $domreltable->appendChild($domrelheader);
  unset($domrelheader);

  // rows
  foreach ($reltable as $term => $relrow) {
    $domrelrow = $dom->createElement('relrow');
    // cells
    foreach ($relheader as $rolspec) {
      $domrelcell = $dom->createElement('relcell');
      if (isset($reltable[$term][$rolspec])) {
        foreach ($reltable[$term][$rolspec] as $topicref) {
          $domref = $dom->createElement('topicref');
          $domref->setAttribute('href', $topicref);
          $domrelcell->appendChild($domref);
          unset($domref);
        }
      }
      $domrelrow->appendChild($domrelcell);
      unset($domrelcell);
    }
    $domreltable->appendChild($domrelrow);
    unset($domrelrow);
  }

  $dom->appendChild($domreltable);

  return $dom->saveXML($domreltable);
}

/**
 * Generates a reltable for a ditamap
 *
 * @param array $nodelist list of node objects
 * @param int $vid vocabulary id
 * @return string the reltable xml
 */
function dita_generate_reltable($nodelist, $vid) {
  $reltable = array();
  $rolspec_types = array();
  foreach ($nodelist as $nid) {
    $node = node_load($nid);
    $rolspec_type = dita_get_doctype($node);
    $terms = dita_get_taxonomy_term($node, $vid);
    //if the node has no terms, we don't do anything with it
     foreach ($terms as $term) {
      //reltable
      $reltable[$term->name][$rolspec_type][] = dita_str_to_filename($node->title) . '_' . $nid . DITA_TOPIC_SUFFIX;
      //collect rolspec types for the relheader
      if (!in_array($rolspec_type, $rolspec_types)) {
        $rolspec_types[] = $rolspec_type;
      }
    }
  }
  return dita_render_reltable($reltable, $rolspec_types);
}


/**
 * Determines the doctype of an xml document
 *
 * @param mixed $node. Can be a DITA node object or an xml string.
 *   If a DITA node object is given, the function will try to get the xml
 *   representation of the node with checking the modules if they implement
 *   dita_body hook.
 * @return string doctype
 */
function dita_get_doctype($node) {
  //@TODO further needs testing

  //It looks like it's a node object
  $body =  $node;
  if (is_object($node)) {
    drupal_alter('dita_body', $body, array(
      'node' => $node,
      'type' => $node->type,
      )
    );

  }
  $xml_object = new DomDocument();
  $xml_object->loadXML($body);
  $dtype = $xml_object->doctype;
  return $dtype->nodeName;
}


/**
 * @todo Document
 */
function dita_get_taxonomy_term($node, $vid) {
  //@TODO Now we assume that the node has only one term... we have to create the
  //the vocabulary with install, and get the term of that vocabulary(?)
  $terms = taxonomy_node_get_terms_by_vocabulary($node, $vid);
  return $terms;
}

/**
 * Export functor for the DITA projects.
 */
class DITAProjectExporter {

  /**
   * The node object of the ditamap.
   *
   * @var stdClass
   */
  protected $node;

  /**
   * The base directory for the conversion.
   *
   * @var string
   */
  protected $basedir;
  protected $orig_basedir;

  /**
   * Prefix of the last conversion.
   *
   * @var string
   */
  public $prefix = NULL;

  public function getBasedir() {
    return $this->basedir;
  }

  public function setBasedir($basedir) {
    $this->basedir = $this->orig_basedir = $basedir;
  }

  public function resetBasedir() {
    $this->basedir = $this->orig_basedir;
  }

  /**
   * @constructor
   *
   * @param stdClass $node
   */
  public function __construct($node) {
    $this->node = $node;
  }

  /**
   * Invokes the functor.
   *
   * This function generates the
   *
   * @param array $vid_array list of vocabulary IDs.
   * 0 means that the reltable generation is disabled.
   * if int is passed, then that will be the id of the only related vocabulary
   * @return string Path for the .ditamap file.
   */
  public function __invoke($vid_array = 0) {

    $vid_array = (array) ($vid_array);
    $this->resetBasedir();

    if (substr($this->basedir, -1) != '/') {
      $this->basedir .= '/';
    }
    $map_content = dita_convert_map_node($this->node, $vid_array);
    $this->prefix = user_password(DITA_PROJECT_PREFIX_LENGTH);
    $map_filename = $this->prefix . '.ditamap';

    $this->basedir .= $this->prefix . '/';

    mkdir($this->basedir);

    $topiclist = dita_get_topiclist($this->node);
    if (is_array($topiclist)) {
      foreach ($topiclist as $nid) {
        $topic_node = node_load($nid);

        $body = $topic_node->body;

        //Generating the body by calling the content type's provider's alter func
        drupal_alter('dita_body', $body, array(
          'node' => $topic_node,
          'type' => $topic_node->type,
          )
        );

        $topic_filename = $this->basedir . dita_str_to_filename($topic_node->title) . '_' . $nid . DITA_TOPIC_SUFFIX;
        file_put_contents($topic_filename, $body);
      }
    }

    //replace placeholders in the map with dynamic data
    $map_content = str_replace('{drupal_path_prefix}', $this->basedir, $map_content);

    file_put_contents($this->basedir . $map_filename, $map_content);
    return $this->basedir . $map_filename;
  }
}

/**
 * Gets all dita nodes from the database
 *
 * @param array $unpublished wether the unpublished nodes should be retrieved
 *
 * @return array of dita nodes (always the revision referenced in node table)
 *   Every element of the array is an associative array. The keys:
 *           nid: node id
 *           title: title of the node
 *           body: dita xml content
 */
function dita_get_topics($unpublished = FALSE) {
  $topics = array();
  $result = db_query("SELECT n.nid, r.body, n.title
                      FROM {node} n
                      INNER JOIN {node_revisions} r
                      ON n.vid = r.vid
                      WHERE n.type = 'dita'
                      AND (status = 1
                      OR n.status = %d)", (int)(!$unpublished));
  while ($record = db_fetch_object($result)) {
    $topics[] = array(
      'nid' => $record->nid,
      'body' => $record->body,
      'title' => $record->title,
    );
  }
  return $topics;
}

/**
 * Gets all simple dita nodes from the database
 *   (if a content type mentioned in an implentation of hook_dita_forminfo()
 *    [the hook is provided by simple_dita_forms] then its nodes are a simple dita nodes)
 *
 * @param array $unpublished wether the unpublished nodes should be retrieved
 *
 * @return array of dita nodes (always the revision referenced in node table)
 *   Every element of the array is an associative array. The keys:
 *           nid: node id
 *           title: title of the node
 *           body: dita xml content
 */
function dita_get_simple_dita_nodes($unpublished = FALSE) {
  if (!module_exists('simple_dita_forms')) {
    return FALSE;
  }
  $topics = array();

  $db_data = array_keys(simple_dita_forms_get_forminfo());

  $db_placeholders = db_placeholders($db_data, 'text');

  $db_data[] = (int)(!$unpublished);

  $sql = "SELECT n.nid,n.title
          FROM {node} n
          WHERE n.type IN ($db_placeholders)
          AND (n.status = 1
          OR n.status = %d)";
  $result = db_query(db_rewrite_sql($sql), $db_data);

  while ($record = db_fetch_object($result)) {
    $topics[] =  array(
      'nid' => $record->nid,
      'title' => $record->title,
      'body' => convert_simple_dita2dita(node_load($record->nid)),
    );
  }
  return $topics;
}

/**
 * Creates a filename for a dita file.
 *
 * @param array $topic
 *           nid: node id
 *           title: title of the node
 *           body: dita xml content
 *
 * @return string
 */
function dita_zip_filename($topic) {
  return check_plain($topic['title']) .'-'. $topic['nid'] .'.xml';
}

/**
 * Creates a zipped file from a list of nodes
 *
 * @param array $topics
 * @return todo
 */
function dita_create_zipfile($topics) {
  if (!array($topics)) {
    return FALSE;
  }
  $file = tempnam("tmp", "zip");
  $zip = new ZipArchive;

  $res = $zip->open($file, ZipArchive::OVERWRITE);
  if ($res) {
    foreach ($topics as $topic) {
      $zip->addFromString(dita_zip_filename($topic), $topic['body']);
    }
    $zip->close();
  }
  else {
    return FALSE;
  }
  return $file;
}

//@TODO Check if the reltable generation is working properly
//@TODO Check if the OpenToolkit converting works correctly while there were
//      multiple changes in the generation method
